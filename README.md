# Rare inversion analysis #

### Approach ###

This simple pipeline is used for rare variant genotyping in the 1000 Genomes samples. The input are the PanGenie genotypes of all 3,202 1000 Genomes samples for the HGSVC SNPs as well as the phased inversion regions for the inversion carrier.
Rare SNPs present in the inversion haplotypes are selected and counted in each of the 3,202 samples. Samples sharing high numbers of SNPs with the samples are selected as potential candidates to carry the inversions.