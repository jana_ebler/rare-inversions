
genotypes = "/gpfs/project/ebler/hgsvc/experiments/genotyping-experiments-revision/genotyping/results-freeze4/merged-vcfs/whole-genome/pangenie_merged_bi_all.vcf.gz"
inversions = "inversions.tsv"
results = 'results'

inversion_to_coordinates = {}
inversion_to_phasing = {}
inversion_to_sample = {}
inversion_to_haplotype = {}

for line in open(inversions, 'r'):
	if line.startswith('variant_id'):
		continue
	fields = line.split()
	inversion_to_sample[fields[0]] = fields[1]
	inversion_to_coordinates[fields[0]] = fields[2]
	inversion_to_phasing[fields[0]] = fields[3]
	inversion_to_haplotype[fields[0]] = fields[4]


rule all:
	input:
		expand("{results}/stats/{inversion}/{inversion}.pdf", results=results, inversion=inversion_to_coordinates.keys()),
		expand("{results}/stats/statistics.tsv", results=results)



# extract rare variants in a region
rule extract_rare_variants:
	input:
		genotypes
	output:
		"{results}/inversion-regions/region-{inversion}.vcf"
	params:
		region = lambda wildcards: inversion_to_coordinates[wildcards.inversion]
	conda:
		"env/inversions.yml"
	shell:
		"bcftools view -r {params.region} {input} | bcftools view --max-af 0.01 > {output}"

# intersect with David's vcfs
rule intersect_phased:
	input:
		region="{results}/inversion-regions/region-{inversion}.vcf",
		phased= lambda wildcards: inversion_to_phasing[wildcards.inversion]
	output:
		"{results}/rare-variants/rare-variants-{inversion}.vcf"
	params:
		haplotype = lambda wildcards: inversion_to_haplotype[wildcards.inversion]
	shell:
		'python3 scripts/intersect-vcfs.py {input.region} {input.phased} "{params.haplotype}"  > {output}'

rule count_variants:
	input:
		"{results}/rare-variants/rare-variants-{inversion}.vcf"
	output:
		"{results}/rare-variants/rare-variants-{inversion}.txt"
	shell:
		"cat {input} | python3 scripts/count-variants.py > {output}"
		
rule prepare_config:
	input:
		inversions
	output:
		"{results}/config.tsv"
	run:
		with open(output[0], 'w') as outfile:
			for line in open(input[0], 'r'):
				fields = line.split()
				fields.append('{result}/rare-variants/rare-variants-{inversion}.vcf'.format(result = wildcards.results, inversion=fields[0]))
				outfile.write('\t'.join(fields) + '\n')

rule compute_statistics:
	input:
		vcfs=expand("{{results}}/rare-variants/rare-variants-{inversion}.vcf", inversion = inversion_to_coordinates.keys()),
		configfile="{results}/config.tsv"
	output: 
		pdf=expand("{{results}}/stats/{inversion}/{inversion}.pdf", inversion=inversion_to_coordinates.keys()),
		tsv="{results}/stats/statistics.tsv"
	params: 
		outname = "{results}/stats"
	conda:
		"env/inversions.yml"
	log:
		"{results}/stats/statistics.log"
	shell:  
		"python3 scripts/compute-statistics.py {input.configfile} {params.outname} 2> {log} 1> {output.tsv}"
