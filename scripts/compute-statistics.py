import sys
import numpy as np
from matplotlib import pyplot as plt
import statistics

def compute_stats(values):
	# compute some stats
	mean = sum(values) / len(values)
	median = statistics.median(values)
	max_v = max(values)
	min_v = min(values)
	sys.stderr.write('Mean: ' + str(mean) + '\n')
	sys.stderr.write('Median: ' + str(median) + '\n')
	sys.stderr.write('Max: ' + str(max_v) + '\n')
	sys.stderr.write('Min: ' + str(min_v) + '\n')


def allele_present(gt_field, gt_index):
	gt = gt_field.split(':')[gt_index]
	if gt == '.':
		return False
	alleles = gt.replace('|', '/').split('/')
	if '1' in alleles:
		return True
	else:
		return False

def analyse_vcf(filename, sample_name, outname, threshold, print_header=True):
	sys.stderr.write('Analysing file ' + filename + ':\n')
	sample_to_ac = []
	sample_to_an = []
	sample_to_ids = []
	samples = []
	sample_index = None
	all_ids = []

	total_variants = 0

	for line in open(filename, 'r'):
		if line.startswith('##'):
			continue
		fields = line.split()
		if line.startswith('#'):
			# extract samples
			samples = fields[9:]
			sample_index = samples.index(sample_name)
			sample_to_ac = [0] * len(samples)
			sample_to_an = [0] * len(samples)
			for s in samples:
				sample_to_ids.append(set([]))
			continue
		assert len(samples) == 3202
		format_field = fields[8].split(':')
		assert 'GT' in format_field
		info_field = { f.split('=')[0] : f.split('=')[1] for f in fields[7].split(';') if '=' in f}
		assert 'ID' in info_field
		varid = info_field['ID']
		all_ids.append(varid)
		gt_index = format_field.index('GT')
		total_variants += 1
		for index, gt_field in enumerate(fields[9:]):
			gt = gt_field.split(':')[gt_index]
			if gt == '.':
				continue
			assert '/' in gt or '|' in gt
			alleles = gt.replace('|', '/').split('/')
			if alleles[0] != '.':
				sample_to_ac[index] += int(alleles[0])
				sample_to_an[index] += 1
				if int(alleles[0]) > 0:
					sample_to_ids[index].add(varid)
			if alleles[1] != '.':
				sample_to_ac[index] += int(alleles[1])
				sample_to_an[index] += 1
				if int(alleles[1]) > 0:
					sample_to_ids[index].add(varid)

	sys.stderr.write('Total variants present in ' + sample_name + ': ' + str(total_variants) + '\n')
	sys.stderr.write('Total samples: ' + str(len(samples)) + '\n')

	# AC stats
	sys.stderr.write('ALT allele per sample (across all variants):\n')
	compute_stats(sample_to_ac)

	#AN stats
	sys.stderr.write('genotyped alleles per sample (across all variants):\n')
	compute_stats(sample_to_an)

	# AF stats
	allele_freq = [ c/float(n) for c,n in zip(sample_to_ac, sample_to_an) ]
	sys.stderr.write('ALT allele frequency per sample (across all variants):\n')
	compute_stats(allele_freq)

	# create histogram of AC counts
	bins = np.arange(0, max(sample_to_ac)+50, 1)
	plt.hist(sample_to_ac, bins=bins)
	plt.title('ALT allele counts per sample')
	plt.xlabel('number of ALT alleles')
	plt.ylabel('count')
	plt.yscale('log')
	plt.savefig(outname + '/' + var_id + '.pdf')
	plt.close()

	header_line = ['variant_id', 'sample', 'nr_rare_variants', 'potential_carriers ' + str(int(threshold*100)) + '% shared alleles)'] + samples
	output_line = [str(var_id), str(sample_name), str(total_variants)]
	potential_carriers = []
	sample_line = []

	total_alleles = sample_to_ac[samples.index(sample_name)]	
	for sample, ac in zip(samples, sample_to_ac):
		sample_line.append(str(ac))
		if (ac / total_alleles) > threshold:
			potential_carriers.append(sample)
	output_line.append(','.join(potential_carriers))
	output_line += sample_line
	if print_header:
		print('\t'.join(header_line))
	print('\t'.join(output_line))


	for sample in potential_carriers:
		index = samples.index(sample)
		ids = sample_to_ids[index]
	
		frame1 = plt.gca()
		frame1.axes.get_yaxis().set_visible(False)

		x_values1 = [ int(s.split('-')[1]) for s in ids  ]
		y_values1 = [ 1 ] * len( x_values1)

		x_values2 = [ int(s.split('-')[1]) for s in all_ids]
		y_values2 = [ 1 ] *  len( x_values2)

		plt.title(sample)
		plt.xlabel('coordinates')
		plt.gcf().axes[0].xaxis.get_major_formatter().set_scientific(False)
		plt.scatter(x_values2, y_values2, color='grey', marker='.', alpha=0.3)
		plt.scatter(x_values1, y_values1, color='red', marker='.', alpha=0.3)
		plt.savefig(outname + '/' + var_id + '-' + sample + '-hits.pdf')
		plt.close()

		with open(outname + '/' + var_id + '-' + sample + '-hits.tsv', 'w') as outfile:
			for var in ids:
				outfile.write(var.strip() + '\n')

inversions = sys.argv[1]
outdir = sys.argv[2]

samples = []
files = []
ids = []

for line in open(inversions, 'r'):
	if line.startswith('variant_id'):
		continue
	fields = line.split()
	samples.append(fields[1])
	files.append(fields[5])
	ids.append(fields[0])

print_header = True
for sample, filename, var_id in zip(samples, files, ids):
	analyse_vcf(filename, sample, outdir + '/' + var_id, 0.7, print_header)
	print_header = False
