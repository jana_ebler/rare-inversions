import sys

total = 0
snps = 0
indels = 0
svs = 0

for line in sys.stdin:
	if line.startswith('#'):
		continue
	fields = line.split()
	info_fields = { f.split('=')[0] : f.split('=')[1]  for f in fields[7].split(';') if '=' in f}
	assert 'ID' in info_fields
	var_id = info_fields['ID']
	var_type = var_id.split('-')[2] # chr15-23373566-DEL-134
	varlen = 1
	if var_type != 'SNV':
		varlen = int(var_id.split('-')[-1])
	if var_type == 'SNV':
		snps += 1
		continue
	elif varlen < 50:
		indels += 1
	else:
		svs += 1

print('nr of snps: ' + str(snps))
print('nr of indels: ' + str(indels))
print('nr of SVs: ' + str(svs))
