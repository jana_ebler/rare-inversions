import sys
from collections import defaultdict

vcf1 = sys.argv[1]
vcf2 = sys.argv[2]
genotype = sys.argv[3]

relevant_vars = defaultdict(lambda: False)

for line in open(vcf2, 'r'):
	if line.startswith('#'):
		continue
	fields = line.strip().split('\t')
	chrom = fields[0]
	pos = fields[1]
	ref = fields[3]
	alt = fields[4]
	assert len(alt.split(',')) == 1
	gt = fields[9].split(':')[0]
	if gt == genotype:
		relevant_vars[(chrom,pos,ref,alt)] = True

for line in open(vcf1, 'r'):
	if line.startswith('#'):
		print(line.strip())
		continue
	fields = line.strip().split('\t')
	chrom = fields[0]
	pos = fields[1]
	ref = fields[3]
	alt = fields[4]

	if relevant_vars[(chrom,pos,ref,alt)]:
		print(line.strip())
